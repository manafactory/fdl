<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Elevation
 */
?>
  <article <?php post_class();?>>
    <?php echo elevation_post_date();?>

    <div class="post-content media-body video">
    
      <?php 
	  $tipo = get_field('tipo_video');
	  $video_id = get_field('video_id');
	  $video_file = get_field('video_file');
      if($video_file or $video_id ){?>
          <div class="post-thumbnail">
              <div class="entry-video">
                  <?php if($tipo == 'youtube') { ?>
                    <iframe width="800" height="600" src="http://www.youtube.com/embed/<?php echo $video_id; ?>" frameborder="0" allowfullscreen></iframe>
                  <?php } else if($tipo == 'vimeo') { ?>
                    <div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/<?php echo $video_id; ?>?title=0&byline=0" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>                  
                  <?php } else if($tipo == 'file') { 
                  	echo '<div class="videobig">';
					echo do_shortcode('[video preload="auto" width="800" height="600" mp4="'.$video_file.'" autoplay="0"][/video]');
					echo '</div>';

                  } ?>
              </div>
          </div>
      <?php } ?>
      
      <h3 class="entry-title"><?php the_title();?></h3><!-- /.entry-title -->
      
      <?php echo elevation_entry_post_meta();?>

      <div class="entry-content">
        <?php the_content();?>
      </div><!-- /.entry-content -->
     
      <?php echo elevation_single_post_entry_footer();?>

    </div><!-- /.post-content -->
  </article><!-- /.post -->
