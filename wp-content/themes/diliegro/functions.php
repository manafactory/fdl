<?php

/*** Child Theme Function  ***/

if ( ! function_exists( 'diliegro_enqueue_scripts' ) ) {
	function diliegro_enqueue_scripts() {
		wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' ); 
	}
	
	add_action( 'wp_enqueue_scripts', 'diliegro_enqueue_scripts' );
}



function delete_post_type(){
  unregister_post_type( 'events' );
}
add_action('init','delete_post_type', 100);



//ACF
if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_video',
		'title' => 'Video',
		'fields' => array (
			array (
				'key' => 'field_5cb9e63cda848',
				'label' => 'Tipo video',
				'name' => 'tipo_video',
				'type' => 'radio',
				'choices' => array (
					'youtube' => 'YouTube',
					'vimeo' => 'Vimeo',
					'file' => 'Media file',
				),
				'other_choice' => 0,
				'save_other_choice' => 0,
				'default_value' => '',
				'layout' => 'horizontal',
			),
			array (
				'key' => 'field_5cb9e670da849',
				'label' => 'Video Id',
				'name' => 'video_id',
				'type' => 'text',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_5cb9e63cda848',
							'operator' => '==',
							'value' => 'youtube',
						),
						array (
							'field' => 'field_5cb9e63cda848',
							'operator' => '==',
							'value' => 'vimeo',
						),
					),
					'allorany' => 'any',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5cb9e691da84b',
				'label' => 'Video File',
				'name' => 'video_file',
				'type' => 'file',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_5cb9e63cda848',
							'operator' => '==',
							'value' => 'file',
						),
					),
					'allorany' => 'all',
				),
				'save_format' => 'url',
				'library' => 'all',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_format',
					'operator' => '==',
					'value' => 'video',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}
