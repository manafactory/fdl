��    s      �  �   L      �	     �	  
   �	  	   �	     �	  	   �	  	   �	     �	  	   
     
     .
     >
  	   F
     P
     j
     s
     |
     �
     �
  '   �
     �
     �
     �
     �
     �
     �
       
          
   %  !   0     R     _     d     j     r     z     ~  
   �     �     �     �     �  F   �  "        $     :     B     Y     g     s     �     �     �     �     �     �     �     �     �     �     �       L        h     w     ~     �     �     �     �  	   �     �     �  	   �  	   �     �                          )     1     7     L     c     o  J   v     �     �     �     �     �  (   �       7   7     o     u     �  	   �     �     �     �     �  )   �     �               4     @  
   L     W     _     b     i    p  $   �  
   �     �  
   �  
   �     �     �     �  
          	     
        &     @     I     Q     Y  $   e  *   �     �     �  	   �  	   �     �     �     �  
                       5     D     M     S     [     b     g  
   t          �  
   �     �  >   �     �     �               0  	   N     X     k     �     �     �     �     �     �     �     �     �     �     �  `   	     j     ~     �     �     �  	   �      �     �     �     �     �     �     
  	   (  	   2     <  	   E  
   O     Z     f     v     �  
   �  W   �     �          	               /     K  >   j  	   �     �     �     �  
   �     �               
     *     <     P     k     ~     �  	   �     �     �  	   �     r   p      d   -   b                     c       R   ?   I             	              '   j   X       K   q       `   a           0   ,   5          Q   6       +   2   Y   #   i   >              f               %         L   e      $   D   s   ;   \                  h   F      M      
   7       =   [   H               P   J       Z       &   ]       B   l              @           !   U          S       1              m   "   g   ^   *   n   o   .       W      <   T       :   _   O                     N       )   8           3       /   E   9       k   (   C   G               4   V   A     donation(s) % Comments % Donated (Edit) 1 Comment Add Image Add New Location Add Video Additional Message : Additional Note Address Address : Address was not specified Amount : Archives Author: Average Awaiting product image Below are the details of your donation. Blog Cancel Comment Category Cause Name : Clear Daily Archives:  Date : Dimensions Donate Donate Now Donation Goal For This Project is Download PDF Edit Email Email * Email : End Event Date: Event End: Event Place: Event Start: Goal: Good How many times would you like this to recur? (including this payment)* How much would you like to donate? I would like to make  In %1$s Invalid Email Address  Invalid Nonce Last Name * Leave a Comment Leave a Comment to %s Lost your password? Monthly Archives:  N/A Name Name * Name : Name of Recipient : Newer Comments News Not that bad Older Comments Only logged in customers who have purchased this product may leave a review. Or Your Amount Pages: Password Perfect Phone Phone : Please fill all required fields Posted by Raised Rate&hellip; Read More Read more Redirecting to paypal Regards, Remember me Reply Required Reviews Sale! Search Results for:  Search Results for: %s Search for: Share: Sorry, this product is unavailable. Please choose a different combination. Start Submit Tag Tags: Thank you for your donation Thank you very much for your donation to There are no reviews yet. This product is currently out of stock and unavailable. To Go Transaction ID : Username or email Very poor View All View All Posts Weight With Would you like to make regular donations? Yearly Archives:  You are donating to : You received a new donation Your rating Your review a one time monthly on weekly yearly Project-Id-Version: 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2019-01-24 09:41+0100
PO-Revision-Date: 2019-03-11 11:13+0100
Last-Translator: 
Language-Team: 
Language: it_IT
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.2.1
X-Poedit-KeywordsList: _e;esc_html__;__
X-Poedit-Basepath: ../../..
X-Poedit-SearchPath-0: plugins/candor-framework
X-Poedit-SearchPath-1: themes/elevation
X-Poedit-SearchPath-2: themes/diliegro
  alla Fondazione Don Luigi Di Liegro % commenti % Donato (Modifica) 1 commento Aggiungi Immagine Aggiungi luogo Aggiungi video Messaggio: Note Indirizzo Indirizzo: Indirizzo non specificato Importo: Archivi Autore: Sufficiente In attesa dell'immagine del prodotto Di seguito i dettagli della tua donazione. Blog Cancella Commento Categoria Progetto: Pulisci Archivi per giorno:  Data: Dimensioni Dona Dona ora Obiettivo da raggiungere Scarica il PDF Modifica Email Email * Email: Fine Data evento: Data fine: Luogo evento: Data inizio: Obiettivo: Buono Quante volte vorresti ripetere la donazione? (inclusa questa)* Quanto vuoi donare? Vorrei donare in %1$s Indirizzo email non valido  Codice  temporaneo non valido Cognome * Lascia un commento Lascia un commento a %s Password dimenticata? Archivi per mese:  N/A Nome Nome * Nome: Destinatario: Commenti successivi Notizie Non male Commenti precedenti Solamente clienti registrati che hanno comprato questo prodotto possono lasciare una recensione. O scegli l'importo  Pagine: Password Perfetto Telefono Telefono: Per favore compila tutti i campi Pubblicato da Raccolti Valuta&hellip; Leggi tutto Leggi tutto Ci stiamo collegando a PayPal A presto, Ricordami Rispondi Richiesto Recensioni In offerta! Risultati per:  Risultati della ricerca: %s Cerca: Condividi: Spiacenti, questo prodotto non è disponibile. Per favore scegli un'altra combinazione. Inizio Invia Tag Tags: Grazie per la tua donazione Grazie per la tua donazione Ancora non ci sono recensioni. Il prodotto non è attualmente in magazzino e non disponibile. Rimanenti ID Transazione: Nome utente o email Scarso Vedi tutte Vedi tutti gli articoli Peso con Vuoi fare donazioni periodiche? Archivi per anno: Aiutaci ad aiutare: Hai ricevuto una donazione La tua valutazione La tua recensione solo ora ogni mese su ogni settimana ogni anno 